#Aula 00 - 06/03

## Problema
Não há maneiras viáveis de aumentar o clock dos computores, a fim de aumentar a velocidade dos computadores.

### Possíveis soluções
* **Hierarquia de memória**: cache e registradores são muito mais rápidos do que usar a memória RAM.
* **Computação paralela**: executar mais instruções ao mesmo tempo.
* **Multicores**: colocar mais unidades de processamentos.
* **Clusters**: juntar vários computadores comuns para fazer um computador paralelo.
* **SIMD**: **S**ingle **I**nstruction **M**ultiple **D**ata.
* **FPGA**: **F**ield **P**rogrammable **G**ate **A**rray
* **OpenMP**, **OpenCL**: Geradores de código através de comentários estruturados