#Aula 01 - 15/03

## Arquitetura de computadores paralelos

Computadores paralelos fazem parte de quase todos os ambientes científicos.

Muitas empresas os empregam para acelerar seus processos.

A tecnologia HPC permeia cada vez mais a arquitetura de computadores pessoais.

## Tipos de paralelismo

* Paralelismo intrínseco (componentes)

* Paralelismo de hardware por função

* Multicore (= Multitask)

* Sistemas multiprocessados (memória compartilhada)

* Sistemas computacionais, clusters (múltiplos computadores)

## Aplicações
Exemplos de aplicações que necessitam de alto desempenho computacional:

* Modelagem física
* Modelagem bioquímica
* Análise de sinais
* Previsão de tempo
* Pesquisa científica em geral
* Órgãos governamentais

## Processamento X Programação

Características do **problema** definem a **arquitetura**:
* **Processamento vetorial**: arquitetura principal dos supercomputado res
* **Multiprocessamento (MPP)**: computadores com processadores massassivamente paralelos
* **Clusters**: arquiteturas compostas por computadores comerciais (servers e workstations)

## Classificação dos computadores paralelos
Uma máquina é caracterizada através da maneira como trata as suas instruções e os seus dados. As categorias são:

* **SISD**: Single Instruction, Single Data
* **SIMD**: Single Instruction, Multiple Data
* **MISD**: Multiple Instruction, Single Data
* **MIMD**: Multiple Instruction, Multiple Data

## Níveis de paralelismo
Divisão a partir de uma atividade única

* Execução de trabalho (job)
* Execução de tarefa (task)
* Execução de função (thread)
* Execução de instrução
* Transferências de registros e dispositivos lógicos (hardware)

## Organização para execução paralela
* Particionamento/granularidade
* Escalamento e distribuição (fork)
* Sincronização
* Comunicação (join)

## Paradigmas de Processamento
* Pipeline
* Phased computation
* Divide and conquer
* Replicated worker

## Granularidade
$`G = \dfrac{Quantidade de computação}{Quantidade de comunicação}`$

### Paralelismo granular
* Fined grained = Blocos pequenos concentrados em múltiplas tarefas
* Coarse grained = Blocos grandes concentrados em poucas tarefas

Quanto mais fina a granularidade, maior o potencial de paralelismo e consequentemente, speed-ups, porém, maior o overhead (comunicação)

A estratégia de particionamento depende do problema, da abordagem e da arquitetura computacional empregada.

## Lei de Amdahl
Interligar computadores em paralelo para realizar uma tarefa implica em coordenar esta solução cooperativa, gerando um overhead de natureza sequencial. Assim, este overhead suplantaria a 
capacidade agregada, estabelecendo um limite superior ao incremento da capacidade de
processamento. 