# Aula 04 - 10/04

## Propriedades

```c
process CS[i=1 to n] {
    while(true) {
        enter protocol
        critical section
        exit protocol
        non critical section
    }
}
```

### Safety
* Exclusão mútua
* Ausência de deadlock
* Se os dois ou mais processos tentam entrar ao menos um entra
* Ausência de atraso desncecessário

### Liveness
* Entrada garantida
* um processo que está aguardando para entrar na seção crítica, vai entrar em algum momento

## 2 Processos

### 1ª tentativa
```c
int turn = 1;

process CS1 {
    while(true) {
        <await (turn == 1);>
        critical section
        <turn = 2>
        non critical section
    }
}

process CS2 {
    while(true) {
        <await (turn == 2);>
        critical section
        <turn = 1>
        non critical section
    }
}
```

* Exclusão mútua $`\Rightarrow`$ **OK**
* Ausência de deadlock $`\Rightarrow`$ **OK**
* Ausência de atraso $`\Rightarrow`$ **NOT OK**

### 2ª tentativa
boolean in1 = false, in2 = false;

```c
process CS1 {
    while(true) {
        <await (!in2); in1 = true;>
        critical section
        in1 = false;
        non critical section
    }
}

process CS2 {
    while(true) {
        <await (!in1); in2 = true;>
        critical section
        in2 = false;
        non critical section
    }
}
```
* Exclusão mútua $`\Rightarrow`$ **OK**
* Ausência de deadlock $`\Rightarrow`$ **OK**
* Ausência de atraso $`\Rightarrow`$ **OK**
* Entrada garantida $`\Rightarrow`$ **justiça forte**

### Generalização
```c
boolean lock = false;

process CSi {
    while(true) {
        <await (!lock) lock = true;>
        critical section
        lock = false;
        non critical section
    }
}
```

#### Como fazer os *awaits*?
1. Usar primitivas de hardware
2. Usar algoritmos mais sofisticados

Instruções do tipo *test-and-set*

```c
boolean PS(boolean lock) {
    <boolean initial = lock;
     lock = true;
     return initial;
    >
}
```

```c
while (lock) skip;
while (PS(lock)) {
    while (lock) skip;
}
```

```c
while(TS(lock)) skip;
```

O primeiro *lock* é mais eficiente, pois evita a invalidação de cache.
