# Aula 03 - 22/03

## Referência crítica
Referência para uma variável modificada por outro processo concorrente.

Se entre dois processos concorrentes não há referências críticas a execução para atômica.

Uma atualização `x=e` satisfaz a propriedade no máximo uma vez se:
1. `e` contém no máximo uma referência crítica, e `x` não é lido por outro processo, ou
2. `e` não contém referências críticas e nesse caso, `x` pode ser lido por outros processos.

## Exemplos de código
```c
<await(B) S;>
```

No exemplo acima, `B` é a condição de sincronização. `S` é uma sequência de comandos.

**IMPORTANTE**: se a condição B respeita a propriedade no máximo uma vez.

O exemplo acima é equivalente:
```c
while(not B)
    skip;
```

## Propriedades
1. **Safety**
  * exclusão mútua
  * ausência de deadlock

2. **Liveness**
  * um processo entrará na seção crítca em algum momento
  * um pedido de serviço será respondido
  * uma mensagem chegará ao destino

## Justiça (Fainess)
São as políticas de escalonamento.

Garantia que todos os processos tem a chance de prosseguir.

Não queremos que a corretude do programa dependa do escalonador do SO.

### Incodicional
Toda ação atômica passível de executção é executada em algum momento.

Não é suficiente para garantir as propriedades. Não garante quando as threads vão executar, nem se executaram corretamente.

### Fraca
* É incodicional.
* Cada ação condicional atômica é executada em algum momento se a condição fica e permanece verdadeira até que seja vista.

A condição pode ser alterada por outra *thread* mais rápida.

### Forte
* É incodicional.
* Toda ação condicional atômica "executável" é executada em algum momento, assumindo que essa condição é frequentemente verdadeira.

É impossível encontrar uam política de justiça forte que seja praticável.

```c
bool continue = true, try = false;

co {
    while (continue) {
        try = false;
        try = true;
    }
}
<await(try) continue = false;>
```
## Problema da seção crítica
Neste problema $`n`$ processos executam repetidamente uma seção critíca e após a seção não critíca.

```c
CS[i=1 to n] {
    while(true) {
        enter protocol
        critical section
        exit protocol
        non critical section
    }
}
```
