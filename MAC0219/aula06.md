# Aula 06 - 16/04

## Algoritmo de Bakery
```c
    int turn[1...n] = ([n]0); // Notação para um vetor com n 0's
    process CS[i = 1 to n] {
        while(true) {
            < turn[i] = max (turn[1...n] + 1) >
            for [j = 1 to N, j != i]
                <await (turn[j] == 0 or turn[i] < turn[j]);>
            /* critical section */
            turn[i] = 0;
            /* non-critical section */
        }
    }
```

## Algoritmo Ticket

```c
    int number = 1, next = 1, turn [1...n] = ([n]0);
    process CS[i = 1 to n] {
        while (true) {
            < turn[i] = number; number++; >
            while (turn[i] != next) skip;
            /* critical section */
            < next = next + 1; >
            /* non-critical section */
        }
    }
```
