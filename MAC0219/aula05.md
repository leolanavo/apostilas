# Aula 05 - 12/04


## Histórias no contexto de Paralela
Uma ordem sequencial em que as instruções de um programa paralelo são executadas.

## Busy Waiting

* Exclusão mútua
* Ausência de deadlock
* Sem atraso desnecessário
* **NÃO** tem entrada garantida

## Algoritmo de Peterson (Tie-breaker)
Usa uma variável adicional para marcar o último a entrar

```c
while(condition) {
    enter protocol
    critical section
    exit protocol
    non critical section
}
```

### Protocolos de entrada

CS1:
```c
while(in2) skip;
in1 = true;
```

CS2:
```c
while(in1) skip;
in2 = true;
```

### Protocolos de saída

CS1:
```c
in1 = false;
```
CS2:
```c
in2 = false;
```

Sem atomicidade não há exclusão mútua, as duas threads podem entrar na seção crítica ao mesmo tempo.

Mesmo que as intruções de mudança do valor de variável sejam atomizadas, não há ausência de deadlock.

### Como melhorar?
Ideia: usar uma variável auxiliar para o caso onde *in1* e *in2* são
verdadeiras.

```c
boolean in1 = in2 = false;

process CS1{
    while (true) {
        int last = 1
        in1 = true
        <await (!in2 or last == 2);>
        critical section
        in1 = false
        non critical section
    }
}

process CS2{
    while (true) {
        int last = 2
        in2 = true
        <await (!in2 or last == 2);>
        critical section
        in2 = false
        non critical section
    }
}
```

## Algoritmo de Decker
Usar uma varíavel compartilhar turnos.

### 1ª Tentativa

#### Protocolos de entrada

CS1:
```c
while (turn != 1) skip;
```

CS2:
```c
while (turn != 2) skip;
```

#### Protocolos de saída

CS1:
```c
    turn = 2;
```

CS2:
```c
    turn = 1;
```

Tem *busy waiting*.

### 2ª tentativa
Variáveis compartilhadas além dos turns

#### Protocolos de entrada

CS1:
```c
while(want2) skip;
want1 = true;
```

CS2:
```c
while(want1) skip;
want2 = true;
```

#### Protocolos de saida

CS1:
```c
    want1 = false;
```

CS2:
```c
    want2 = false;
```

Sem **exclusão mútua**


### 3ª tentativa

#### Protocolos de entrada

CS1:
```c
want1 = true;
while(want2) skip;
```

CS2:
```c
want2 = true;
while(want1) skip;
```

#### Protocolos de saida

CS1:
```c
    want1 = false;
```

CS2:
```c
    want2 = false;
```

Tem **deadlock**

### 4ª tentativa
Desistir de entrar se houver conflito.

#### Protocolos de entrada

CS1:
```c
want1 = true;
while (want2) {
    want1 = false;
    skip;
    want1 = true;
}
```

CS2:
```c
want2 = true;
while (want1) {
    want2 = false;
    skip;
    want2 = true;
}
```

Tem **atraso desnecessário**

### Algoritmo de Decker
Usar uma varíavel interna *turn = 1*

#### Protocolo de entrada
CS1:

```c
want1 = true;
while(turn2) {
    if (turn == 2) {
        want1 = false;
        while (turn != 1) skip;
        want1 = true;
    }
}
```

#### Protocolo de saída
```c
turn = 2;
want1 = false;
```

## Dá pra usar apenas 2 variáveis?
Sim, usando o algoritmo de Manna-Pnueli

### Protocolo de Entrada
CS1:
```c
<if (want2 == -1)
     want1 = -1;
 else
     want1 = 1;>

while(want1 == want2) skip;
```

#### Protocolos de saída
CS1:
```c
want1 = 0;
```
