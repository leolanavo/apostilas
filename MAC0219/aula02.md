# Aula 02 - 20/03

## Encontrar o máximo

### Código sequencial
```c++
int n = SIZE;
int m = 0;
for (int i = 0; i < n; i++) {
    if (a[i] < m)
        m = a[i];
}
```

Para paralizar devemos
* atomizar a seção crítica:

```c++
if (a[i] < m)
    m = a[i];
```

* compartilhar a variável m

* Usar `forall` ao invés de `for`. Este comando executa o paralelismo automáticamente.

Toda vez que a trocamos o valor `m`, invalidamos o cache de todos os outros *cores*. Então podemos fazer:

```c++
if (a[i] < m)
    if (a[i] < m)
        <if (a[i] > m)
            m = a[i];>
```

Fazendo essa cadeia de `if's` repetidos evitamos a invalidação constate do cache.

## Propriedades de um programa
Atributo verdadeiro para todas as execuções parciais.

1. Safety: o programa nunca entra em um estado ruim.

2. Liveness: em algum momento o programa entra num estado bom.

**NOTA**:

* **Estado bom**: o programa finaliza com sucesso. Todos os processos tem a chance de executarem as ações necessárias para completar o programa.
* **Estado ruim**: o programa trava em *deadlock*. Pelo menos dois processos, travam um ao outro, entra em *loop* infinito.

### Como verficar as propriedades
* **Teste/depuração**: inviável, só verfica um número limitado de casos.
* **Análise exaustiva**: inviável, existem muitos estados.
* **Análise abstrata**: análise empiríca do programa.

## Ações atômicas e comanando *await*
Duas formas de obtermos "ordens" corretas:

1. Combinando ações de blocos atômicos $`\Rightarrow`$ **exclusão mútua**
2. Atrasando a execução até que uma certa condição seja válida $`\Rightarrow`$ **condição de sincronização**