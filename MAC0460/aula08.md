# Aula 08 - 15/04

## Transformações Não-Lineares
```math
x = (x_0, ..., x_d)  \Rightarrow^{\Phi} z = (z_0, z_1, ..., z_{d'})
```

Cada $`z_i = \Phi_i(x)`$, ou seja, $`z = \Phi(x)`$.

Mesmo que façamos a transformação, a função alvo, sempre estará no espaço $`X`$, ou seja:
```math
    g(\Phi(x)) = w'^T\Phi(x)
```
### Qual o preço quando fazemos uma transformação linear
Quando fazemos a aprendizado em $`x`$, conseguimos um vetor $`w`$, que possui dimensão menor que $`w'`$. Ou seja, estamos aumentando a dimensão VC.

Um bom ponto é que reduzimos o $`E_{in}`$ para 0, porém com alto custo, e perda de generalização.

## Data Snooping
Olhar para o dataset, antes de escolher o modelo, pode ser perigoso para a generalização, pois tendenciamos o modelo para se encaixar o melhor possível no dataset.

A fazer *data snooping* contaminamos os dados.

## Regressão Logística
Para qualquer modelo linear, primeiro calculamos o produto interno $`s`$

```math
s = \sum_{i = 0}^d w_ix_i
````

* **Classificação Linear**: $`h(x) = sign(s)`$
* **Regressão Linear**: $`h(x) = s`$
* **Regressão Logística**: $`h(x) = \theta(s)`$

$`\theta`$ é a função probabilística, ou seja o resultado está entre 0 e 1, e será chamada de **função logística**.

```math
\theta(s) = \frac{e^s}{1 + e^s}
```

Ao usarmos a função $`\theta`$ ao invés da $`sign`$, usamos um **soft threshold**, a classificação não é mais binária, mas agora, é a probabilidade de um resultado ser *bom*.

Em geral, o dataset não contém a probabilidade dado as características, mas sim o resultado binário dado as características, *bom* ou *ruim*. Então, $`P(y|x) \backsim Bernoulli(f(x))`$, e queremos descobrir $`f(x)`$.

```math
g(x) = \theta(w^T x) \approx f(x)
```

### Avaliação do erro
A avaliação do erro é plausível e baseada em *likelihood*: se $`h = f`$, o quão provável que eu chegue em $`y`$, a partir de $`x`$?

```math
h(x) = \theta(w^T x) \Rightarrow P(y|x) = \theta (y w^T x)
```

Note que: $`\theta (-s) = 1 - \theta (s)`$

Então podemos definir *likelihood* como:

```math
\Pi_{n=1}^N P(y_n | x_n ) = \Pi_{n=1}^N \theta(y_n w^T x_n)
```

Se maximizarmos a *likelihood*, minimizaremos o erro.

Maximizar $`\Pi_{n=1}^N \theta(y_n w^T x_n) \Rightarrow`$ Minimizar $`-\frac{1}{N}ln\left(\Pi_{n=1}^N \theta(y_n w^T x_n)\right)`$, então:

```math
E_in(w) = \frac{1}{N} \sum_{n=1}^N ln(1 + e^{-y_n w^T x_n})
```
Esta avaliação do erro é chamada **"cross-entropy" error**

### Algoritmo de aprendizado
Será iterativo, pois a avaliação do erro não tem uma fórmula fechada.

A função $`E_in`$ é uma função convexa (formato de V).

Usaremos a técnica de **gradiente descendente**: $`w(t+1) = w(t) + \eta v`$

```math
\Delta E_{in} = E_{in}(w(0) + \eta v) - E_{in}(w(0)) =  ^{(1)}
```
```math
= n \nabla E_{in}(W(0))^T + O(\eta ^2)
\geq^{(2) \text{e} (3)} -\eta ||\nabla E_{in}(w(0))||
```

(1) Expansão da série de Taylor

(2) Desconsiderando o erro

(3) $`v = -\frac{\nabla E_{in}(w(0))}{||\nabla E_{in}(w(0))||}`$

A **tava de aprendizado** ($`\eta`$) precisa ser equilibrada, para o melhor aproveitamento do algoritmo.

```math
\Delta w = - \eta \nabla E_{in}(w(0))
```