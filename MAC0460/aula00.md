# Aula 00 - 26/02

## Avisos:
* Inscreva-se no PACA
* Ver o curso do Mastafa (**Learning from Data**)
* Avaliação:
  1. Exercícios
  2. Projetos
  3. Provas (**talvez**)
* Monitores: Felipe e Caio (alunos de doutorado)

## Máquinas

**Máquinas**: processam dados
  dados de entrada (1) ---> dados de saída | (1) processamento (algritmo)

**MachineLearning** tenta achar o algritmo para resovler problemas

## Como reconhecer um número
### Problema
Reconhecer um número escrito por uma pessoa.

### Ideias
1. Calcular a distância entre os pontos escuros na matriz
2. Gerar um conjunto de _características_ dos números, e usar uma função de
   similiaridade para comparar a imagem com a base de dados

## Definição de Machine Learning

Mitchell definiu **ML** em 1997, em seu livro **Machine Learning**, em três componentes:
* T: Trabralho
* E: Experiência
* D: Desempenho

O desempenho da máquina melhora com a execução das tarefas, ou seja, pela
experiência.

## Quando aplicar ML?

**ML** é aplicável quando o problema a ser resolvido possui três características:
1. Existe um padrão
2. Não há uma definição "_matemática_" formal do padrão
3. Existe um volume grande de dados

**OBS**: Em **ML** pode não haver saída.

## Exemplo
**Problema**: Aprovação de crédito, o banco deseja saber se é viável aprovar o aumento do limite do cartão de crédito do usuário

**Entrada**: Dados sobre o usuário que quer aplicar, por exemplo:
* Salário
* Quando tempo trabalho
* Quanto deve ao banco
* etc

**Saída**: Aprovado ou reprovado

**Dados**: histórico do banco

### Definições

* **Função Alvo**: uma função *f*: *X* --> *Y*, *X* é o conjunto de características do usuário e *Y* é o resultado da aprovação. Essa função é o alvo ideal ao usarmos **ML**.

* **Função aproximada**: uma função *g* que se aproxima de *f*, *g* é o resultado do processo de **Learning**.

* **Conjunto de hipóteses**: É um conjunto de funções que podem vir a ser a **função aproximada**.

* **Algoritmo de Aprendizado**: é o algoritmo que usa os dados para gerar/melhorar a **função aproximada**

* **Modelo de Aprendizado**: é o conjunto **algoritmo de aprendizado** +
**conjunto de hipóteses**

### Perceptron

Os dados precisam ser linearmente independentes

É um **conjunto de hipóteses**

Calcula um valor dado o conjunto de características da entrada. $`\sum^d_{i=1} w_ix_i \gt t`$ onde $`x_i`$ é um vetor características do dado de entrada, $`w_i`$ é o peso da característica, e $`t`$ é o valor que denota o limite entre ser aprovado ou não.

```math
	h(x) = sign\left( \sum^d_{i=1}x_iw_i - t\right)
```

O que define um $`h(x)`$ é o conjunto de pesos $`w_i`$ e o limiar $`t`$

Para simplificar a notação usaremos um $`x_0 = 1`$ e um $`w_0`$, o qual ficará no lugar de $`t`$ na fórmula, dessa forma, temos que:


```math
	h(x) = sign\left( \sum^d_{i=1}x_iw_i - t\right) = sign\left(\sum^d_{i=1}w_ix_i + w_0\right) = sign\left(\sum^d_{i=0}w_ix_i\right) = sign(w^\intercal x)
```

Um ponto foi classificado erroneamente, se $`sign(w^\intercal x_n) \neq y_n`$, se isto ocorre, a correção é feita da seguinte forma $`w \leftarrow w + y_nx_n`$

Uma iterção do **PLA** (*Perceptron Learning Alghorithm*) pega um ponto classificado erroneamente, e corrige o vetor de pesos.

### Tipos de Learning

* **Supervisionado**: os dados são do tipo (entrada, saída correta)
* **Não supervisionado**: os dados são do tipo (entrada, ?)
* **Por reforço**: os dados são do tipo (entrada, alguma saída, nota da saída)

## Próxima aula
1. Ver a _lecture_ 01
2. Ver o slide
3. Ler a seção **1.1** e **1.2** do livro
