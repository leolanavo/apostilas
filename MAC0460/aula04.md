# Aula 04 - 01/04

## Medições de Erro

### In-sample
```math
E_{in}(h) = \frac{1}{N}\sum^n_{n=1}e(h(x_n), f(x_n))
```

### Out-of-sample
```math
E_{out}(h) = E_x[e(h(x), f(x))]
```

## Testes VS Treinamento

### Testing
```math
    P[|E_{in} - E_{out}| > \epsilon] \leq 2e^{2\epsilon^2N}
```

### Training
```math    
    P[|E_{in} - E_{out}| > \epsilon] \leq 2Me^{2\epsilon^2N}
```

### Exemplo
Um professor dá uma lista de exercícios para os alunos, para eles praticarem antes da prova final.

* **Treinamento**
  * $`E_{in} \Rightarrow`$ nota da prova final
  * $`E_{out} \Rightarrow`$ entendimento da matéria

* **Treinamento**
    * $`E_{in} \Rightarrow`$ nota da lista de exercícios
    * $`E_{out} \Rightarrow`$ entendimento da matéria
    * No caso do treinamento, o estudante praticamente memorizou algumas respostas, então seu aprendizado está contaminado. Essa contaminação é representada pelo $`M`$ na esquação.

### De onde vem o $`M`$
Vamos definir como *bad events*, eventos nos quais o $`E_{in}`$ não se aproximou o suficiente do $`E_{out}`$.

$`M`$ é o número total de *bad events*.

```math
P[B_1\ or\ B_2\ or\ ...\ or\ B_m] \leq P[B_1] + P[B_2] + ... + P[B_M]
```

No lado direito da equação acima, assumimos que não há intersecção entre os $`B_i`$.

### Melhorar $`M`$

Porém, na prática, os *bad events* intersectam bastante entre si, ou seja:

```math
    |E_{in}(h_1) - E_{out}(h_1)| \approx |E_{in}(h_2) - E_out(h_2)|
```

Ao invés de consideramos todo o espaço de entrada, iremos só usar um conjunto finito de amostras. Fazendo isso, a número de hipotéses deixa de ser infinito, e torna-se o número de hipóteses que criam classificações diferentes dentro da amostra de treinamento, essas hipóteses são chamadas de **dicotomias**.

Definindo matemáticamente, um hipótese é: 

```math
h : X \Rightarrow \{-1, +1\}
```

E uma dicotomia:

```math
h : {x_1, x_2, ..., x_N} \Rightarrow \{-1, +1\}
```

$`H`$ é o conjunto de todas as infinitas hipóteses, e $`H(x_1, ... x_N)`$ é o conjunto de todas as ditocomias.

### Função de crescimento
A função de crescimento conta a maior número de ditocomias dado $`N`$ pontos.

```math
m_H(N) = max_{x_1, ..., x_N \in X} |H(x_1, ... , x_N)|
```

No caso, de algoritmos de classificação, podemos afimar $`m_H(N) \leq 2^N`$

Dizemos que um conjunto de hipóteses foi quebrado quando $`m_H(N) = 2^N`$

Se $`m_H(N)`$ for polinomial, ao substituirmos $`M`$ por $`m_H(N)`$ na inequação, a probabilidade pode tornar-se pequena facilmente, se aumentarmos suficientemente o tamanho da amostra.

### Break point
Quando nenhum conjunto de dados de tamanho $`k`$ pode quebrar $`H`$. Nesse caso, $`k`$ é um *break point*. Qualquer $`x \geq k`$ também é um *break point*.

Se houver algum *break point*, então $`m_H(N)`$ é polinomial em $`N`$.