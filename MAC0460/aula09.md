#Aula 09 - 17/04

## Gradiente descendente estocástico
Pegamos uma amostra $`(x_n, y_n)`$. E aplique GD em $`e(h(x_n), y_n)`$.

Pegamos a direção "média":

```math
E_n \left[- \nabla e(h(x_n), y_n)\right] = \frac{1}{N} \sum_{n=1}^{N} - \nabla
e(h(x_n), y_n)

= - \nabla E_{in}
```

Para uma tupla $`(x_n, y_n)`$ aleatória.

### Vantages
1. Mais barato computacionalmente
2. Aleatório
3. Evita os minímos locais, em geral

### Rule of Thumb
Para a taxa de aprendizado inicial, $`\eta = 0.1`$ é bom.

## Modelo de Redes Neurais

O modelo foi baseado no modelo biológico da rede neural de um humano.

### Perceptrons

Combinaremos perceptrons em camadas para o primeira iterção.

Não podemos pular camadas, e a saída de uma camada sempre vai para a próxima
camada, nunca para a anterior.

#### Red Flags
Se aumentarmos o número de perceptrons, podemos peder generalizção.

A otimização de cada perceptron é compolicada se o dataset não é linear
independente.

## Backpropagation
