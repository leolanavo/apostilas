# Aula 02 - 07/03

## Transformação Não-Linear

Podemos aplicar uma transformação não-linear nos dados, para tornar-los **linearmente independentes**, tornando possível aplicar algoritmos de aprendizado linear.

Após aplicarmos o algoritmo linear, realizamos a transformação não-linear **inversa** na reta separadora, e obtemos o seprador **não-linear** originalmente desejado.

```math
x = (x_1, x_2, ... , x_d) \overset{\alpha}{\Rightarrow} z = (z_0, z_1, ..., z_d'),
```
onde $`\alpha`$ é uma transformação não-linear, e quando $`z_i`$ pode ser uma combinação não-linear de vários $`x_j`$.

Os $`y_i`$ não mudam, eles continuam iguais. E oss pesos, $`w_i`$, são definidos somente em $`Z`$ e não em $`X`$.

A fórmula de $`g(x)`$ fica: $`g(x) = sign(w'^{\top}\alpha(x))`$

## Medidas de erro
Mede o quão bem $`h(x) \approx f(x)`$, $`E(h, f)`$

Definição sobre **ponto**

```math
e(h(x), f(x)) = (h(x) - f(x))^2e(h(x), f(x)) = [h(x)]
```

**in-sample** error:
```math
E_in(h) = \dfrac{1}{N} \sum_{n=1}^N e(h(x_n), f(x_n))
```

**out-of-sample**
```math
E_out(h) = E_x\left[ e(h(x_n), f(x_n))\right]
```

Há dois tipos de erro em problemas binários: *false accept* ou *false reject*. Como penalizar cada tipo de erro?

### Problema
Supermercado aceita digital para usar discontos.

*false reject* é bem mais custoso do que *false accpet*. O primeiro pode levar a perda de um cliente, equanto o segundo, faz o supermercado dar um disconto. Neste caso, aumentamos a penalização para o *false reject*.

Isso nos mostra que a a penalização depende da aplicação.

## Ruído
A **função alvo** nem sempre é uma função matemática, pois há a possibilidade de dois pontos idênticos resultarem em pontos diferentes.

Dado isto, a partir de agora usaremos uma **distribuição alvo**, definida por:
```math
P(y | x) = \dfrac{P(y) \cap P(x)}{P(x)}
```
Agora o par $`(x, y)`$ é dado pela distribuição conjunta:
```math
P(x)P(y | x)
```
A **função alvo** é definida por $`f(x) = E(y | x)`$ mais o ruído $`y - f(x)`$.

Caso o alvo seja **determinístico**, temos que: $`P(y | x) = 0, se y \neq f(x)`$

## Probabilidades
* **$`P(x)`$**: probabilidade desconhecida que selecionou o **conjunto de dados de treino**. Se treinarmos o algoritmo com esses dados, qual a probabilidade deles aparecerem se escolhermos um dado aleatóriamente de **TODOS** os dados. O quão importante é $`x`$.

* **$`P(y|x)`$**: é o que estamos tentando aprender. Algumas vezes na literatura acharemos $`P(x)P(y|x) = P(x, y)`$.
