# Aula 01 - 04/03

## Perceptron
Vimos anteriormente, que o algoritmo **PLA** não lida bem com valores linearmente inseparáveis. O algoritmo entra em um *loop* infinito, já que não como traçar uma reta separadora entre os conjuntos de dados.

### Alternativa
Podemos definir um número **fixo** de iterações que o algoritmo irá executar. Porém, ao impormos essa limitação, **não garantimos** que a hipótese final, será a que mais se aproxima da **função alvo**.

Para resolver o problema acima, o algoritmo **pocket** foi criado. O **pocket** é um **PLA** com número fixo de iterações e com uma medição de X entre Y, e ao final das iterações, retorna a **melhor** hipótese segundo o critério.

## Regressão linear
São funções cujo resultado final é um valor dentro dos $`\mathbb{R}`$

A saída de um algoritmo de **regressão linear** é do tipo:

```math
h(x) = \sum_{i=0}^d w_i x_i
```

O sinal do resultado é sempre importante para os algoritmos lineares

### Problema
Decidir quanto crédito dar ao cliente do banco.

Temos $`N`$ pares do tipo  $`(x_i, y_i)`$, onde $`x_i`$ é aplicação do cliente e $`y_i`$, um número real, é o crédito dado àquele cliente.

### Como medir o erro?
Pela fórmula do erro quadrático,

```math
E(x) = (h(x) - f(x))^2
```

**in-sample** error, pode ser definido como o erro da estimativa em relação a reposta correta, dentro dos dados de treinamento:
```math
E_{in}(h) = \frac{1}{N}\sum^N_{n=1}(h(x_n) - y_n)^2,
```

**out-of-sample** error é o erro da estimativa em relação a resposta correta, fora dos dados de treinamento.

usaremos essa fórmula para minimizar o erro da **regressão linear**.

```math
E_{in}(w) = \tfrac{1}{N} Xw - y^2
```

onde $`X `$ é a matriz com todos os vetores $`x_i`$ e $`y`$ é o vetor com todos os valors $`y_i`$, a fim de minimizar $`E_{in}(w) `$, primeiro obteremos o gradiente da mesma:

```math
E_{in}(w) = \frac{2}{N} X^{\top}(Xw - y) = 0_v \Rightarrow X^{\top}Xw = X^{\top}y \Rightarrow,
```
```math
w = X^{\alpha}y, \text{where } X^{\alpha} = (X^{\top}X)^{-1}X^{\top}
```


$`X^{\alpha}`$ é a **pseudo-inversa**, mesmo não $`X`$ não sendo inversível, se fizermos $`X^{\alpha}X = I`$, porém $`XX^{\alpha}`$ não resulta na **identidade**.

### Usar regressão linear para classificação
Dá pra usar **regressão linear** para problemas de classificação, porque $`\pm 1`$ são números reais, logo,  $`w^{\top}x_n \approx y_n = \pm 1`$. Então é quase certo que $`sign(w^{\top}x_n)`$ coincida com $`y_n`$.

## Dados Não-Lineares
Certas características afetam o resultado de uma forma **linear**, por exemplo, **anos na mesma residência** diminui o efeito no crédito que o usuário receber depois de 5 anos.

Os $`w_i`$ são nossas variáveis lineares, e os $`x_i`$ são constantes determinadas pelos dados.