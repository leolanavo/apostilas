# Aula 06 - 12/04

## Cálculo Relacional de Tuplas
O **Cálculo Relacional de Tuplas** (CRT) é uma alternativa à Álgebra Relacional
(AR).

Enquanto a AR é procidemental, o CRT é declarativo, i. e., o CRT nos permite
descrever um conjunto de respostas sem explicar como elas são computadas.

O CRT influenciou fortemente o SQL.

Uma linguagem de consulta é considerada **relacionalmente completa** se esta
expressar qualquer consulta que possa ser realizada em CRT.

### Consultas
Em CRT, a consulta tem forma $`{t | P(t) }`$, isto representa o conjunto de
todas as duplas *t*, para as quais o predicado *P* é verdadeiro.

* *t* é uma variável de tuplas.
* *P* é uma expressão condicional.
* *t.A* ou *t[A]* denota o valor do atributo *A* da tupla *t*.
* *EMPREGADO(t)* é o mesmo que $` t \in EMPREGADO`$.

### Exemplos
Para recuperar todos os empregados com salário acima de 5000.

```math
{ t | EMPREGADO(t) AND t.SALARIO > 5000 }
```

Para recuperar apenas o nome e sobrenome dos empregados com salários acima de
5000.

```math
{ t.PNOME, t.SNOME | EMPREGADO(t) AND t.SALARIO > 5000 }
```
### Átomos
Uma fórmula é feita de átomos, e átomos podem ser:
* $`R(t_i)`$, onde *R* é uma relação e $`t_i`$ é uma variável de tupla.
* $`t_i.A op t_j.B`$, onde $`op \in {=, <, \leq, >, \geq, \neq}`$
* $`t_i.A op c`$, onde *c* é uma constante.

Cada átomo só pode resultar em **TRUE** ou **FALSE**.

Para o átomo $`R(t_i)`$ é **TRUE** se $`t_i \in R`$.

Átomos pode ser compostos usando **AND**, **OR** e **NOT**. Também podemos usar
usar implicações $`\Rightarrow`$ e $`Leftrightarrow`$.

```math
X \Rightarrow Y \equiv (NOT X) OR Y
X \Leftrightarrow Y \equiv (X \Rightarrow Y) AND (Y \Rightarrow X)
```

### Quantificadores Universais e Existenciais
Uma fórmula pode possuir quantificadores :

* $`\forall`$: quantificador universal
* $`\exists`$: quantificador existencial

Nas clásulas $`\forall t1`$ ou $`\exists t2`$ são variáveis de tupla vinculadas.
Se não forem vinculadas, serão livres.

Sendo F uma fórmula:
* $`(\forall(t))(F(t))`$ será TRUE se F for TRUE para todas as tupla t no
  universo.
* $`(\exists(t))(F(t))`$ será TRUE se F for TRUE para pelo menos uma tupla t.

### Mapeando AR para CRT

#### Projeção
```math
\Pi_{SNOME, PNOME SALARIO} (EMPREGADO)
\equiv
{ t.SNOME, t.PNOME, t.SALARIO | EMPREGADO(t) }
```

#### Seleção
```math
\Sigma_{SEXO=F} (EMPREGADO)
\equiv
{ t | EMPREGADO(t) AND (t.SEXO=F)}
```

#### Join
```math
DEPARTAMENTO JOIN_{DNUM = NDEP} EMPREGADO
\equiv
{ t | EMPREGADO(t) AND (\exists d) (DEPARTAMENTO(d) AND d.DNUM = t.NDEP) }
```

#### Divisão


### Consultas complexas
Para todos os projetos localizados em Houston, liste o número do projeto, o
número do departamento que o controla e o nome do seu gerente.

```math
{ p.PNUM, p.DNUM, m.PNOME |
PROJETO(p) AND EMPREGADO(m) AND p.LOCALIZACAO='Houston' AND
(\exists d) (DEPARTAMENTO(d) AND
d.DNUMERO = p.DNUM AND
d.GERNSS = m.NSS) }
```
