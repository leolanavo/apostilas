# Aula 01 - 01/03

## Revisão da última aula
Antes de começar implementar um sistema, deve-se modelar os dados e as funcionalidades

A abstração de dados é usada para modelar os dados

Classe = conjunto com vários elementos com características semelhantes

Instância = elemento do conjunto

Ao segmentar os dados em classes, estas devem ser **disjuntas** e **enumeráveis**

As classes estabelecem um relacionamento **intra** entre os elementos e a classe

As classes estabelecem relacionamentos **inter** com outras classes

## Modelo Entidade-Relacionamento (MER)

**MER** é um modelo de dados de alto-nível criado com o objetivo de representar a semântica associada aos doados do minimundo (dados)

O modelo é utilizado para na fase do **projeto conceitual**

O resultado de aplicar o **MER** é o **DER**, **D**iagrama **E**ntidade-**R**elacionamento

### Atributos
As características de uma instância são **atributos**:
  * Endereço de uma pessoa
  * NUP de um aluno

Podem haver atributos **multivalorado**, por exemplo, telefone

Atributos **derivados** deve ser obtidos após o processamento dos dados, por exemplo, contagem de alunos

Alguns atributos podem ser nulos

**Atributo-chave** é o atributo que funciona como identificador da **entidade** dentro do **tipo de conjunto**

### Entidades

**Tipo de entidade** é equivalente à classe, e **entidade** à instância

**Entidade-fraca** é uma entidade que não tem atributo-chaves. Entidades que só podem ser identificadas através da associação com uma outra entidade

### Relacionamento
O **relacionamento** é uma associação entre duas ou mais entidades, é uma classe **mapeadora**

O **grau** de um tipo de relacionamento é o número de **tipos de entidades** envolvidas

Um **relacionamento** pode ser um **atributo**, por exemplo, **EMPREGADO TRABALHA\_PARA DEPARTAMENTO**

Um **relacionamento** pode ter **atributos**

#### Restições de Relacionamento

A **razão de cardinalidade** especifica a quantidade de instâncias de relacionamento em que quantidade pode participar
  * **1:1**: uma pessoa só tem um cpf
  * **1:N**: uma pessoa pode ter *N* telefones
  * **N:N**: *N* pessoas podem ter *N* amigos

A **participação** específica se a existência de uma entidade depende dela estar relacionada com outra entidade através de um
relacionamento


## Dúvidas
