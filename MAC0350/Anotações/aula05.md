# Aula 05 - 10/04

## Álgebra Relacional

A álgebra relacional contempla as operações que permitem especificar consultas
sob relações

As operações são dividas em dois grupos:
* Operações de Teoria dos Conjuntos
* Operaçãoes desenvolvidas especificamente para bancos de dados.

## Operadores

### SELECT - $`\sigma`$
Seleciona, segundo alguma condição, tuplas de uma relação.

É um operador unário, ou seja, só seleciona tuplas de uma relação.

O grau da relação resultante é o mesmo grau da relação original.
fb6e
A operação é comutativa: ao invés de usar o SELECT em cascata, podemos usar o
operador lógica AND.

Abaixo selecionaremos os empregados que trabalham no departamento nº 4

```math
\sigma_{NDEP = 4} (EMPREGADO)
```

#### Condições
Para especificar as operações, podemos utilizar:
* Valor constante
* Nome do atributo
* Operadores relacionais $`\Rightarrow`$ {$`=, <, \leq, \geq, \neq`$}
* Operadores lógicos $`\Rightarrow`$ {**AND**, **OR**, **NOT**}

#### Fórmula geral
```math
\sigma_{**<condição>**} (**<RELAÇÃO>**)
```

### PROJECT - $`\pi`$
Seleciona colunas de uma relação, removendo as todas as outras tuplas não especificadas.

A relação resultante terá no máximo o mesmo número de atributos da relação original.


Não é comutativa.

```math
\pi_{NOME, SALÁRIO} (EMPREGADO}
```

### Fórmula Geral
```math
\pi_{**<A1>**,..., **<A2>** } (**<RELAÇÃO>**)
```

## Sequência de operações
Podemos combinar operações para realizar consulta mais complexas.
```math
\pi_{NOME, SALÁRIO} (\sigma_{DEP=5}(EMPREGADO)
```

Na consulta acima, criamos uma relação intermediária, através do **SELECT**, e após isso filtramos o **nome** e **salário** de somente os empregados que trabalham no departamento 5.

## Renomeando atributos
```math
RESULT_{(NOME, SOBRENOME, SALÁRIO)} \leftarrow \pi_{PNOME, SNOME, SALÁRIO} (DEP5_EMPS)
```

## Operaçãoes da Teoria dos Conjuntos
Os operadores de Teoria dos Conjuntos aplicam-se ao modelo relacional pois uma
relacional pois uma relação é como um conjunto de tuplas.

### Exemplo
Recuperar o NSS dos empregados que trabalham no departamento, ou, indiretamente
supervisionam empregados que trabalham no departamento 5.

```math
DEP5_EMPS \leftarrow \sigma_{NDEP=5}(EMPREGADO)
RESULT1 \leftarrow \pi_{NSS}(DEP5_EMPS)
RESULT2(NSS) \leftarrow \pi_{NSSSUPER} (DEP5_EMPS)
RESULT \leftarrow RESULT1 \cup RESULT2
```

### Operadores
* $`A \cup B`$: União (todas as tuplas de A e todas de B)
* $`A \cap B`$: Intersecção (todas as tuplas comunns de A e todas de B)
* $`A - B`$: Diferença (todas as tuplas de A que não estão em B)
* $`A \times B`$: Produto Catersiano (combinação das tuplas de A com as de B)

União, intersecção e diferença preciam que a A e B sejam compatíveis na união.

### Operador JOIN

Combina informações de duas ou mais relações.

Pode ser definido como um produto catersiano, seguido de uma seleção.

### Operador EQUIJOIN
Operação JOIN com somente comparações de igualdade.

Problema: produz atributos com valores iguais na relação resultante.

### Operador NATURAL JOIN
É um EQUIJOIN em que os atributos repitidos são removidos.

### Operador DIVISION
É útil para descobrir tuplas que se relacionam outras tuplas. Por exemplo:
Recuperar todos os empregados que trabalham com John Smith em algum projeto.

1. Pegamos todos os projetos que John Smith trabalha.
2. Obter a relação com os NSS e o número do projeto.
3. Pegar os NSSs que trabalham em todos os departamentos que John Smith trabalha.


