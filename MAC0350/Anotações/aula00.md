# Aula 00 - 27/02

## Avisos
* **Linguagens**: Python e SQL
* **Projeto**: vale a nota final, vai ter fases intermediárias
* **Nota**: Projeto, provas

###Provas
* **MP** = 2\*P1 + 3\*P2)/5
* **NI**: nota do projeto
* **MF**: 0.6\*MP + 0.4\*NI

### Datas
* **P1**: 19/04
* **P2**: 24/05
* **Projeto**: 14/06

**OBS**: só tem *PSUB* para quem tem problemas pessoais

## MAC0350

Engenharia de Softare **+** Banco de dados, o que une as duas áreas é a
**abstração de dados**

**Modelagem** --> **Implementação**

**Modelagem** é criar um modelo conceitual do problema, usando algum método.

**Método** é abstração usada na modelagem do problema, por exemplo, **Orientação a Objetos**.

O **mundo** que tentamos modelar é caótico, por isso tentatamos extrair dois mundos, **mundo funcional** (*Eng. Soft*), do qual retiramos as funcionalidades do sistema, e o **mundo de estrutura de dados** (*BD*), do qual retiramos os dados e sua organização.

### Os Dez Mandamentos
1. Adorarás as abstrações de dados sobre todas as coisas
2. Não projetarás bancos de dados baseado no modelo físico
3. Projetarás primeiramente as classes "naturais"; (não cairás na tentação de classes complexas originárias de relacionamentos e muito menos confudirás atributos com classes e, ainda, relacionamentos com classes)
4. Não confudirás relacionamentos binários com triplos
5. Não confudirás relacionamento triplo com agregação
6. Não confudirás composição lógica com física
7. Compreenderás a evolução dos relacionamentos nas hierarquias de especialização e composição
8. Não utilizarás o mapeamento de das abstrações de dados em vão (Conhecerás as formas de mapeamento objeto-relacional. Não cairás na tentação de mapeamento fúteis)
9. Não levantarás falso testemunho sobre o valor semântico dos dados (Não ignorarás a capacidade do usuário como solucionador de problemas na identificação do valor semântico dos dados)
10. Rejeitarás a tentação do mundo estático

### Abstração de dados
**Definição**: Processo mental que seleciona algumas propriedades de um conjunto de objetos e exclui outras irrelevantes em um dado contexto.

O **problema** é definir quantas e quais classes terão em seu sistema.

O primeiro passo de construir um sistema é usar **abstração de classificação**, para criar/entender as classes do seu sistema.

#### Classificação
Podemos responder à pergunta: "**Quais as classes?**", como mais duas perguntas:
  1. Quais os dados?
  2. O que desejar fazer com os dados?

Expor os dados, o mundo do seu sistema, o **mapa de dados**

Particionar o mundo disjuntamente, de acordor com algum critério de similaridade

Depois definir os conjuntos, nomeamos os conjuntos, e numeramos seus elementos

Intersecções entres os conjuntos são tratadas como exceções

Esses conjuntos serão as **classes concretas**, as classes mais próximas do mundo real


#### Definições
* **Classe**: conjunto de objetos descritos pelo mesmo conjunto de propriedades
* **Tipo**: conjunto de propriedades que descreve os objetos de uma classe
* **Instância**: elementos de uma **Classe**

#### Abstração por Classificação
* Define um conceito abstrato a partir do levantamento das propriedades comuns a uma coleção de objetos do mundo real
* Estabelece um relacionamento do tipo "*é membro de* entre a classe e suas instâncias

#### Realcionamento
Existe relacionamento **intra** entre elemento e classe

Pode existir um relacionamento **inter** entre classes

O **vínculo** entre classes, pode ser dado de diferentes formas, principalmente por restrições.
## Tarefas
* Ler a apostila `Python-SQL` no PACA, principalmente o **primeiro** e
  **segundo** capítulos.
