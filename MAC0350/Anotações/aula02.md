# Aula 02 - 06/03


## Definições de classes
* **Classes naturais**: são as primeiras classes que aparecerm no modelagem do *mini-mundo*. Estabelecem relações INTRA com seus elementos.

* **Classes de relacionamento**: Estabelece uma relação INTER com outras classes.

## Relacionamentos

* **4° Mandamento**: Não confudirás relacionamento binário com ternário

No esquema acima, os relacionamentos binários ("cadastra", "tutor" e "possível") não conseguem relacionar as três entidades da mesma forma que o relacionamentos ternário ("matrícula") faz.

Um aluno que seja tutorado de um professor, não necessariamente faz a matéria cadastrada pelo professor.

Porém no relacionamento ternário, estabelecemos que para o aluno se matricular na matéria, precisou de um professor associado à matéria.

## Modelo Relacional
Representa os dados da base de dados como uma coleção de relações

### Definições
#### Tabelas
* **Relação esquema**: a tabela, definição de como os dados serão guardados.
* **Relação**: conjunto de tuplas.
* **Atributo**: coluna da tabela, uma característica do tipo de entidade.
* **Tupla**: linha da tabela, uma entidade.
* **Domínio**: todos os possíveis valores para um atributo.
* **Grau de relação**: número de atributos numa relação esquema.

### Base de dados
* **Esquema da base de dados relacional**: conjunto das relações esquema
* **Instância da base de dados relacional**: conjunto de relações do BD
* **Base de dados relacional**: o esquema do BD e suas relações

#### Chaves
* **Atributo-chave**: conjunto de atributos que não possuem valoração repetida na relação
* **Super-chave**: conjunto de atributos que identificam uma tupla unicamente.
* **Chave**: uma **super-chave** que se retiramos qualqer atributo do conjunto, não conseguimos mais identificar uma tupla unicamente.
* **Chave-candidata**: quando numa relação esquema há mais de uma chave, cada uma dessas chaves é chamada de **chave-candidata**.
* **Chave-primária**: uma das chaves-candidata
* **Chave-estrangeira**: é uma chave que identifica uma tupla em outra relação esquema, usada para relacionar tabelas

#### Restrições de Integridade
* **Integridade referencial**: o SGBD mantém a integridade das chaves-estrageira
* **Integridade de entidade**: nenhum valor de chave-primária pode ser nulo, para manter sua
unicidade.

### Notações
* Esquema de relação $`R`$ de grau $`n`$ $`\Rightarrow R(A_1, A_2, ..., A_n)`$
* Tupla $`t`$ em uma relação $`r(R) \Rightarrow t=<v_1, v_2, ..., v_n>`$, onde $`v_i`$ é o valor $`A_i`$
* Valor do atributo $`A_i`$ tupla $`t`$, $`t[A_i] = <'v_i> `$ 
