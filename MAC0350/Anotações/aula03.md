# Aula 03 - 08/03

## Mapeamento
É comum, em projetos lógicos de BD, realizar a modelagem dos dados através de um de dados de alto-nível.

O produto desse processo é o esquema do BD.

O modelo de dados de alto-nível normalmente adotado é o MER e o esquema do BD especificado em MR.

## Passos do Mapeamento

### Passo 1
* Para cada **tipo de entidade normal E** no DER, crie uma **relação R**, que inclua todos os **atributos simples** de E.

* Inclua também os atributos simples dos **atributos compostos**.

* Escolha um dos atributos-chave de E como a **chave-primaria** de E.

* Se a chave escolhida é composta, então os atributos simples da chave formarão a chave-primaria.

### Passo 2
* Para cada **entidade fraca W** do DER com o **com o tipo de entidade de identificação E**, crie uma **relação R** e inclua todos os atributos simples, e os atributos simples que compõem os atributos compostos.

* Inclua como a **chave-estrangeira** de R a chave-primaria da relação que corresponde ao **tipo de entidade proprietário da identificação**.

* A chave-primária de R é a combinação da chave-primaria do tipo de entidade proprietário da identificação e a chave-parcial do tipo de entidade fraca W.

### Passo 3
**NOTA**: **R** denota relacionamento nos passos 3 a X, e não mais relação.

* Para cada tipo de **relacionamento binário 1:1, R**, do DER, identifique as relações S e T que correspondem aos tipos de entidade que participam de R.

* Escolha uma das realações e inclua como chave-estrangeira de S a chave-primaria de T.

* Inclua todos os atributos simples (ou os atributos simples de atributos compostos) do tipo de relacionamento 1:1, R, como atributos de S.

### Passo 4
* Para cada tipo de **relacionamento binário regular 1:N** (não fraca), R, identificar a relação S que representa o tipo de entidade que participa do lano N de R.

* Inclua como chave-estrangeira de S a chave-primária de T que representa o outro tipo de entidade que participa em R.

* Inclua também quaisquer atributos simples (ou atributos simples de atributos compostos) do tipo de relacionamento 1:N, como atributo de S.

### Passo 5
* Para cada tipo de **relacionamento binário M:N, R**, crie uma nova relação S para representar R.

* Inclua como chave-estrangeira de S as chaves-primárias das relaçôes que representam os tipos de entidade participantes; sua combinação irá formar a chave-primaria de S.

* Inclua também qualquer atributo simples do tipo de relacionamento M:N (ou atributos simples dos atributos compostos) como atributos de S.

### Passo 6
* Para cada atributo A multivalorado, crie uma nova relação R que inclua o atributo A e a chave-primaria, K, da relação que representa o tipo de entidade ou o tipo de relacionamento que tem A como atributo.

* A chave-primaria de R é a combinação de A e K.

* Se o atributo multivalorado é composto inclua os atributos simples que o compõem.

### Passo 7
* Para cada tipo de relacionamento n-ário, R, n>2, crie uam nova relação S para representar R.

* Inclua como chave-estrangeira em S as chaves-primárias das relaçôes que representam os tipos de entidades participantes.

* Inclua também qualquer atributo simples do tipo de relacionamento n-ário (ou atributos simples dos atributos compostos) como atributo de S.

* A chave-primária de S é normalmente a combinação de todas as chaves-estrangeiras que referenciam as relaçôes que representam os tipos de entidades participantes.
