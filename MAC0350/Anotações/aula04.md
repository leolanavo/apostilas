# Aula 04 - 05/04

## Agregação
São relações entre classes que nem sempre ocorrem. Por exemplo, um candidato que
é entrevistado por um empresa, nem sempre gera um emprego.

## Especialização
Uma classe mais geral pode ser extendida para uma classe mais específica.

Uma pessoa pode ser estudante ou professor.

Se a especialização é **total**, então toda instância da classe geral, precisa
se espcializar. O oposto é **parcial**.

Se a especialização é **disjunta**, então uma mesma instância da classe geral
não pode estar em duas espcializações simultaneâmente, ou seja, as
expecializações são mutuamente excludentes. O oposto é **sobreponível**.
